package pl.edu.agh.ki.bd.htmlIndexer.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Sentence {
    private long id;
    private List<Word> words = new ArrayList<>();
    private ProcessedUrl processedUrl;
    private String content;



    public Sentence() {

    }

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public List<Word> getWords() {
        return words;
    }
    public void setWords(List<Word> words) {
        this.words = words;
    }

    public ProcessedUrl getProcessedUrl() {
        return processedUrl;
    }

    public void setProcessedUrl(ProcessedUrl processedUrl) {
        this.processedUrl = processedUrl;
    }

    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }
}
