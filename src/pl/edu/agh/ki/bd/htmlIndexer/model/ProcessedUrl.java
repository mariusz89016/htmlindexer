package pl.edu.agh.ki.bd.htmlIndexer.model;

import java.util.*;

public class ProcessedUrl {
    private long id;
    private String url;
    private Date date;
    private Set<Sentence> sentences = new HashSet<>();
    private Set<ImageLink> imageLinkSet = new HashSet<>();

    public ProcessedUrl() {
    }

    public ProcessedUrl(String url, Date date) {
        this.url = url;
        this.date = date;
    }

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }

    public Date getDate() {
        return date;
    }
    public void setDate(Date date) {
        this.date = date;
    }

    public Set<Sentence> getSentences() {
        return sentences;
    }
    public void setSentences(Set<Sentence> sentences) {
        this.sentences = sentences;
    }

    public Set<ImageLink> getImageLinkSet() {
        return imageLinkSet;
    }
    public void setImageLinkSet(Set<ImageLink> imageLinkSet) {
        this.imageLinkSet = imageLinkSet;
    }

}
