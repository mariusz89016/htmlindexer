package pl.edu.agh.ki.bd.htmlIndexer;

import java.io.IOException;
import java.util.*;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import pl.edu.agh.ki.bd.htmlIndexer.model.ImageLink;
import pl.edu.agh.ki.bd.htmlIndexer.model.ProcessedUrl;
import pl.edu.agh.ki.bd.htmlIndexer.model.Sentence;
import pl.edu.agh.ki.bd.htmlIndexer.model.Word;
import pl.edu.agh.ki.bd.htmlIndexer.persistence.HibernateUtils;

public class Index {
    public void indexWebPage(String url) throws IOException {
        Session session = HibernateUtils.getSession();
        Transaction transaction = session.beginTransaction();

        ProcessedUrl processedUrl = new ProcessedUrl();
        processedUrl.setDate(new Date());
        processedUrl.setUrl(url);
        session.persist(processedUrl);
        Document doc = Jsoup.connect(url).get();
        Elements elements = doc.body().select("*");

        for (Element element : elements) {
            if (element.ownText().trim().length() > 1) {
                Sentence sentence = new Sentence();
                for (String sentenceContent : element.ownText().split("\\. ")) {
                    String[] words = StringUtil.toWords(sentenceContent);
                    for (String word : words) {
                        Word tmp = (Word) session.get(Word.class, word.trim().toLowerCase());
                        //new
                        if(tmp==null) {
                            tmp = new Word(word.trim().toLowerCase());
                            session.persist(tmp);
                            sentence.getWords().add(tmp);
                        }
                        //existing
                        else {
                            tmp.getSentences().add(sentence);
                            sentence.getWords().add(tmp);
                            session.persist(tmp);
                        }
                    }
                }
                sentence.setContent(element.ownText().trim());
                processedUrl.getSentences().add(sentence);
                session.persist(sentence);
            }
        }

        Elements images = doc.body().select("img");
        for (Element image : images) {
            ImageLink imageLink = new ImageLink(image.absUrl("src"));
            session.persist(imageLink);
            processedUrl.getImageLinkSet().add(imageLink);
        }
        session.persist(processedUrl);

        transaction.commit();
        session.close();
    }

    public Map<Sentence, Long> findSentencesByWords(String sentence) {
        Session session = HibernateUtils.getSession();
        Transaction transaction = session.beginTransaction();

        String[] words = StringUtil.toWords(sentence);

        List<Object[]> result = session.createQuery("select sentence, count(sentence) from Word w inner join w.sentences sentence where w.content IN (:words) group by sentence order by count(sentence) desc").setParameterList("words", words).list();
        Map<Sentence, Long> output = new LinkedHashMap<>();
        for (Object[] item : result) {
            output.put((Sentence) item[0], (Long) item[1]);
            ((Sentence) item[0]).getWords().size();
        }
        transaction.commit();
        session.close();

        return output;
    }

    public List<Sentence> findSentencesLongerThanLength(int length) {
        Session session = HibernateUtils.getSession();
        Transaction transaction = session.beginTransaction();

        Long lengthOfString = (long) length;

//        List<String> result = session.createQuery("select s.content from Sentence s where length(s.content) > :lengthOfString").setParameter("lengthOfString", length).list();
        List<Sentence> result = session.createQuery("select distinct s from Sentence s inner join fetch s.words word where (select sum(length(word.content)) from s.words word) > :lengthOfString").setParameter("lengthOfString", lengthOfString).list();

        transaction.commit();
        session.close();

        return result;
    }

    public List<ProcessedUrl> getProcessedUrls() {
        Session session = HibernateUtils.getSession();
        Transaction transaction = session.beginTransaction();

        //1. left join fetch!
        //      List<ProcessedUrl> result =  session.createQuery("select p from ProcessedUrl p left join fetch p.sentences").list();
        //2. wyciaganac session do gory
        //3. wrzucac do metody session
        List<ProcessedUrl> result =  session.createQuery("select p from ProcessedUrl p").list();

        transaction.commit();
        session.close();
        return result;
    }

    public void printStatistics() {
        Session session = HibernateUtils.getSession();
        Transaction transaction = session.beginTransaction();

        List<Object[]> result = session.createQuery("select s.processedUrl.url, s.processedUrl.date, sum(s) from Sentence s group by s.processedUrl").list();
        System.out.format("%35s | %11s | %7s\n", "URL", "DATE", "COUNT");
        System.out.println("-----------------------------------------------------------");
        for (Object[] item : result) {
            System.out.format("%35s | %11s | %7s\n", item[0], item[1], item[2]);
        }
        System.out.println("-----------------------------------------------------------");

        transaction.commit();
        session.close();
    }

    public int countOccurrenceOfWord(String word) {
        Session session = HibernateUtils.getSession();
        Transaction transaction = session.beginTransaction();

        String[] wordList = StringUtil.toWords(word);
//        BigInteger amount = (BigInteger) session.createSQLQuery("select count(sentenceId) from SentenceWord where wordId=:word group by wordId").setParameter("word", word).list().get(0);
        int amount = session.createQuery("select s from Word w inner join w.sentences s where w.content in (:word)").setParameterList("word", wordList).list().size();
        transaction.commit();
        session.close();
        return amount;
    }

    public void printLetterStatistics() {
        Session session = HibernateUtils.getSession();
        Transaction transaction = session.beginTransaction();

        List<Word>wordList = session.createQuery("select s.words from Sentence s").list();

        int max = 0;
        Map<Character, Integer> characterIntegerMap = new HashMap<>();
        for (Word word : wordList) {
            for (char c : word.getContent().toCharArray()) {
                if(characterIntegerMap.containsKey(c)) {
                    Integer integer = characterIntegerMap.get(c);
                    characterIntegerMap.put(c, ++integer);
                    if(integer>max) {
                        max = integer;
                    }
                }
                else {
                    characterIntegerMap.put(c, 0);
                }
            }
        }

        for (char c : characterIntegerMap.keySet()) {
            int amount = characterIntegerMap.get(c);
            float amountOfStripes = amount/(float)max * 60;
            System.out.print(c + ": ");
            for (int i = 0; i < amountOfStripes; i++) {
                System.out.print("|");
            }
            System.out.println();
        }
        transaction.commit();
        session.close();
    }

    public List<ImageLink> getImageLinks() {
        Session session = HibernateUtils.getSession();
        Transaction transaction = session.beginTransaction();

        List<ImageLink> result = session.createQuery("from ImageLink").list();

        transaction.commit();
        session.close();
        return result;
    }

}

class StringUtil {
    public static String[] toWords(String sentence) {
        String[] words = sentence.split("\\s+");
        for (int i = 0; i < words.length; i++) {
            words[i] = words[i].replaceAll("[., ]", ""); //"[^\\w]"
        }

        return words;
    }
}
