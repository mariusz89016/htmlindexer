package pl.edu.agh.ki.bd.htmlIndexer;

import java.io.*;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.edu.agh.ki.bd.htmlIndexer.model.ImageLink;
import pl.edu.agh.ki.bd.htmlIndexer.model.ProcessedUrl;
import pl.edu.agh.ki.bd.htmlIndexer.model.Sentence;
import pl.edu.agh.ki.bd.htmlIndexer.model.Word;
import pl.edu.agh.ki.bd.htmlIndexer.persistence.HibernateUtils;

public class HtmlIndexerApp
{

	public static void main(String[] args) throws IOException
	{
		HibernateUtils.getSession().close();

		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		Index indexer = new Index();

		while (true)
		{
			System.out.println("\nHtmlIndexer [? for help] > : ");
			String command = bufferedReader.readLine();
	        long startAt = new Date().getTime();

			if (command.startsWith("?"))
			{
				System.out.println("'?'      	 - print this help");
				System.out.println("'x'      	 - exit HtmlIndexer");
				System.out.println("'i URLs'  	 - index URLs, space separated");
				System.out.println("'f WORDS'	 - find sentences containing all WORDs, space separated");
				System.out.println("'fl INTEGER' - find sentences longer than specified argument");
				System.out.println("'p'          - print processed URLs with words");
				System.out.println("'s'          - print statistics");
				System.out.println("'c WORDS'    - count words occurrences");
                System.out.println("'l'          - print letter statistics");
                System.out.println("'o'          - output image links to file \"output.html\"");
            }
			else if (command.startsWith("x"))
			{
				System.out.println("HtmlIndexer terminated.");
				HibernateUtils.shutdown();
				break;
			}
			else if (command.startsWith("i "))
			{
				for (String url : command.substring(2).split(" "))
				{
					try {
						indexer.indexWebPage(url);
						System.out.println("Indexed: " + url);
					} catch (Exception e) {
						System.out.println("Error indexing: " + e.getMessage());
					}
				}
			}
            else if (command.startsWith("fl "))
            {
                int length = Integer.parseInt(command.substring(2).trim());
                System.out.println("Found sentences where content longer than: " + length);
                for (Sentence sentence : indexer.findSentencesLongerThanLength(length))
                {
                    int amountOfChars = 0;
                    System.out.print(sentence.getId() + "  ");
                    for (Word word : sentence.getWords()) {
                        amountOfChars += word.getContent().length();
                        System.out.print(word.getContent() + " ");
                    }
                    System.out.println("\n   Ilość znaków: " + amountOfChars);
                }
			}
			else if (command.startsWith("f "))
			{
                Map<Sentence, Long> map = indexer.findSentencesByWords(command.substring(2));

				for (Sentence sentence : map.keySet())
				{
                    long amount_matched = map.get(sentence);
                    System.out.print("Zdanie:\n\t");
                    for (Word word : sentence.getWords()) {
                        System.out.print(word.getContent() + " ");
                    }
                    System.out.println("\nDopasowano: " + amount_matched);
                    System.out.println("----------------------------");
                }
			}
            else if (command.startsWith("p"))
            {
                Session session = HibernateUtils.getSession();
                Transaction transaction = session.beginTransaction();

                for (ProcessedUrl processedUrl : indexer.getProcessedUrls())
                {
                    processedUrl = (ProcessedUrl) session.merge(processedUrl);
                    System.out.println("<" + processedUrl.getDate() + " " + processedUrl.getUrl() + " >");
                    for (Sentence sentence : processedUrl.getSentences()) {
                        System.out.print("\t");
                        for (Word word : sentence.getWords()) {
                            System.out.print(word.getContent() + " ");
                        }
                        System.out.println();
                    }
                }
                transaction.commit();
                session.close();
            }
            else if (command.startsWith("s"))
            {
                indexer.printStatistics();
            }
            else if (command.startsWith("c"))
            {
                int amount = indexer.countOccurrenceOfWord(command.substring(2));
                System.out.println("Occurrence '" + command.substring(2) + "': " + amount);
            }
            else if (command.startsWith("l"))
            {
                indexer.printLetterStatistics();
            }
            else if (command.startsWith("o"))
            {
                PrintStream out = new PrintStream(new FileOutputStream("output.html"));
                List<ImageLink>imageLinkList = indexer.getImageLinks();
                out.append("<html>");
                for (ImageLink imageLink : imageLinkList) {
                    out.append("<img src=\"").append(imageLink.getUrl()).append("\"/>");
                }
                out.append("</html>");
                out.close();
            }

            System.out.println("took " + (new Date().getTime() - startAt) + " ms");

		}

	}

}
